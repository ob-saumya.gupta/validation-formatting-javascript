"use strict;";
var fs = require('fs');
var readline = require('readline');
var separatorRegex = /[-*\\/\s]/g;


var filepath = "amount-test.csv"; // Replace with the actual file path

function formatting(filepath) {
  var rl = readline.createInterface({
    input: fs.createReadStream(filepath),
    crlfDelay: Infinity,
  });

  rl.on('line', (line) => {
    var columns = line.split(',');
    var value = columns[0];
    var isFinal = columns[2].trim();
    var separator = ",";
    var formattedValue = amount(value,isFinal,separator);
    // var formattedValue = inputAadhaarFormatting(value, " ");
    console.log(`Input-Value: ${value} and Formatted-Value: ${formattedValue}`);
    console.log();
  });
}



function formatter(DateString, separator) {
    var formatDate = "";
    formatDate = Date(DateString, separator);
    return formatDate;
}

// This function is designed to format a given date string (DateString) based on the provided separator (separator).
// DateString (string): The input date string that needs to be formatted. It represents the date in various formats, such as "dd", "ddMM", "ddMMyyyy", etc.
// separator (string): The separator that should be used to format the date. It could be any character, such as "/", "-", ".", etc.
// The function returns the formatted date as a string. 
function Date(DateString, separator) {

        var dateSeparatorRegex = new RegExp("[-.\\\\/\\s]");
        //if (dateRegex.IsMatch(DateString) || (isFinal && DateString.Length == 10))
    
        // cases accounted dd or ddMM or ddMMyyyy
        if (!dateSeparatorRegex.test(DateString) && DateString.length > 2 && DateString.length < 9)
        {
            if (DateString.length > 2)
                DateString = DateString.substring(0, 2) + separator + DateString.substring(2);
            if (DateString.length > 5)
                DateString = DateString.substring(0, 5) + separator + DateString.substring(5);
        }
        var formattedDate = ""; // Initialize an empty string to store the formatted date.
        var DateString = DateString.replace(/[-.\\\\/\\s]/g, separator); // Replace all occurrences of common date separators with the provided separator.
    
        // Check if the DateString contains the separator. If it does, then the date format is segmented by the separator.
        // if (DateString.includes(separator)) {
            var dateParts = DateString.split(separator); // Split the DateString into an array of parts based on the separator.
            var day, month, year;
    
            // Determine the values for day, month, and year based on the number of parts obtained from the split.
            if (dateParts.length == 3) {
                day = dateParts[0];
                month = dateParts[1];
                year = dateParts[2];
            } else if (dateParts.length == 2) {
                day = dateParts[0];
                month = dateParts[1];
            } else if (dateParts.length == 1) {
                day = dateParts[0];
            }
    
            // Add a leading "0" to day and month if they are single-digit numbers.
            if (day != null && day < 10) {
                day = "0" + day;
            } 
            if (month != null && dateParts[1].length != 2 && month < 10) {
                month = "0" + month;
            }
    
            // Concatenate day, month, and year to form the formatted date based on the available values.
            if (day != null && month != null && year != null) {
                formattedDate = day + separator + month + separator + year;
            } else if (day != null && month != null) {
                formattedDate = day + separator + month; 
                //this block is used to handle cases like 13/012 ==> 13/01/2
                if(formattedDate.length > 5){
                    formattedDate = day + separator + formattedDate.substring(3,5) + separator + formattedDate.substring(5);
                }
            } else {
                formattedDate = day;
            }
    return formattedDate;
}

// This function is designed to format a given Aadhaar number string (inputAadhaarString) 
// by inserting a specified separator at regular intervals to create a formatted Aadhaar number.
// inputAadhaarString (string): The input Aadhaar number string that needs to be formatted. It may contain "-" and space characters, which will be removed during formatting.
// separator (string): The separator that should be used to format the Aadhaar number. It could be any character, such as "/", "-", ".", space, etc.
// Return Value: The function returns the formatted Aadhaar number as a string. 
function inputAadhaarFormatting(inputAadhaarString, separator) {
  // Remove all occurrences of "-" and space from the input string
  inputAadhaarString = inputAadhaarString.replace(/\D/g, "");

  var formattedAadhaarNumber = '';
  var chunkSize = 4;

  for (var i = 0; i < inputAadhaarString.length; i += chunkSize) {
      // Add the separator after every chunk of digits
      if (i !== 0) {
        formattedAadhaarNumber += separator;
      }

      // Add the chunk of digits to the formattedCardNumber
      formattedAadhaarNumber += inputAadhaarString.substr(i, chunkSize);
  }

  // Use Array.prototype.join() to join the array into a string with the specified separator
  return formattedAadhaarNumber;
}

// This function is designed to format a given mobile number string (inputMobileString) 
// inputMobileString (string): The input mobile number string that needs to be formatted. It may contain "-" and space characters, which will be removed during formatting.
// separator (string): The separator that should be used to format the mobile number. It could be any character, such as "-", ".", space, etc.
// Return Value: The function returns the formatted mobile number as a string. 
function inputMobileFormatting(inputMobileString, separator) {
    // Remove all occurrences of "-" and space from the input string
    inputMobileString = inputMobileString.replace(/[- ]/g, "");

    // Initialize an empty string to hold the formatted mobile number
    var formattedMobileNumber = '';

    // Loop through each character in the inputMobileString
    for (var i = 0; i < inputMobileString.length; i++) {
        // Check if the current index is at positions 3 or 6 (after every three digits)
        if (i === 3 || i === 6) {
            // If so, add the specified separator (e.g., "-") to the formattedMobileNumber
            formattedMobileNumber += separator;
        }
        // Add the current character from inputMobileString to the formattedMobileNumber
        formattedMobileNumber += inputMobileString.charAt(i);
    }

    // Return the formatted mobile number
    return formattedMobileNumber;
}

// This function is designed to format a given name string (inputNameString) 
// by capitalizing the first letter of each word in the name. 
// nputNameString (string): The input name string that needs to be formatted.
// Return Value: The function returns the formatted name as a string. 
function inputNameFormatting(inputNameString) {
    return inputNameString.replace(/\b\w/g, (match) => match.toUpperCase());
}

// This function is designed to format a given PAN (Permanent Account Number) 
// string (inputPanString) by converting all characters to uppercase.
// inputPanString (string): The input PAN string that needs to be formatted.
// The function returns the formatted PAN string as a string. 
function inputPanFormatting(inputPanString) {
    inputPanString = inputPanString.replace(separatorRegex, "");
    return inputPanString.toUpperCase();
}

// This function is designed to format a given card number string (inputCardString) by removing all non-digit characters and 
// inserting a specified separator at specific positions to create a formatted card number.
// inputCardString (string): The input card number string that needs to be formatted. It may contain non-digit characters (e.g., letters, special characters), which will be removed during formatting.
// separator (string): The separator that should be used to format the card number. It could be any character, such as "-", ".", space, etc.
// The function returns the formatted card number as a string. 
function inputCardFormatting(inputCardString, separator) {
  // Remove all non-digit characters from the input string
  inputCardString = inputCardString.replace(/\D/g, '');

  var formattedCardNumber = '';
  var chunkSize = 4;

  for (var i = 0; i < inputCardString.length; i += chunkSize) {
      // Add the separator after every chunk of digits
      if (i !== 0) {
          formattedCardNumber += separator;
      }

      // Add the chunk of digits to the formattedCardNumber
      formattedCardNumber += inputCardString.substr(i, chunkSize);
  }

  return formattedCardNumber;
}


// This function is designed to format a given input amount string (inputString) representing a monetary amount.
// It separates thousands, lakhs, and crores places with a specified separator (separator) and adds trailing ".00"
// inputString (string): The input amount string that needs to be formatted. It is expected to be a numeric string with or without decimal places.
// isFinal (boolean): A flag that indicates whether the amount formatting is considered final. If set to true, ".00" will be added to the formatted amount if it does not already have a decimal part.
// separator (string): The separator that should be used to format the amount. It could be any character, such as ",", ".", space, etc.
// Return Value: The function returns the formatted amount as a string.
// function amount(inputString, isFinal, separator) {
//     const inputArray = inputString.split(".");
//     const charArray = inputArray[0].split("").reverse();
//     inputString = formatGenericString(charArray.join(""), [3, 2, 2, 2, 1], separator);
//     inputString = inputString.split("").reverse().join("");
  
//     if (inputArray.length > 1) {
//       inputString += "." + inputArray[1];
//     } else if (isFinal) {
//       inputString += ".00";
//     }
  
//     return inputString;
// }
function amount(inputString, isFinal, separator) {
  inputString = inputString.replace(/\D/g, "");
    const inputArray = inputString.split(".");
    let formattedString = "";
  
    for (let index = inputArray[0].length - 1; index >= 0; index--) {
      if (inputArray[0][index] === separator) {
        continue;
      }
      
      if (index === 0 || index === inputArray[0].length - 1 || (inputArray[0].length - index) % 2 === 0) {
        formattedString = inputArray[0][index] + formattedString;
      } else {
        formattedString = separator + inputArray[0][index] + formattedString;
      }
    }
  
    if (inputArray.length > 1) {
      formattedString += "." + inputArray[1];
    } else if (isFinal) {
      formattedString += "." + "00";
    }
  
    return formattedString;
}
  
  
// This function is a generic utility function used to format a given input string (inputString) 
// based on a specified display format. The function adds separators at specified positions 
// in the input string according to the provided displayFormat array.
// inputString (string): The input string that needs to be formatted.
// displayFormat (array of integers): An array that specifies the positions where separators should be added in the input string.
// separator (string, optional): The separator character that should be used when formatting the string. If not provided, it defaults to a space character.
// The function returns the formatted string as a result.
// function formatGenericString(inputString, displayFormat, separator = " ") {
//     inputString = inputString.replace(separatorRegex, '');  

  
//     const maxLengthWithoutSeparator = displayFormat.reduce((sum, length) => sum + length, 0);
//     const filteredInputString = inputString.replace(new RegExp(separator, "g"), "");
  
//     let formattedString = "";
//     let separatorsAdded = 0;
  
//     for (let i = 0; i < displayFormat.length; i++) {
//       const separatorAfter = displayFormat[i];
  
//       for (let charIndex = 0; charIndex < separatorAfter; charIndex++) {
//         const index = charIndex + separatorsAdded;
//         if (index >= filteredInputString.length) {
//           break;
//         }
//         formattedString += filteredInputString.charAt(index);
//       }
  
//       separatorsAdded += separatorAfter;
  
//       if (separatorsAdded === maxLengthWithoutSeparator || separatorsAdded >= filteredInputString.length) {
//         break;
//       }
  
//       formattedString += separator;
//     }
  
//     return formattedString;
// }

function formatGenericString(inputString, displayFormat, separator = " ") {
    let formattedString = "";
    let separatorLocation = Number.MAX_SAFE_INTEGER;
    // displayFormat = displayFormat.toString();
    if (displayFormat.length > 0) {
      separatorLocation = displayFormat.toString().charCodeAt(0);
    }
    
    let i = 0;
    for (let idx = 0; idx < inputString.length; idx++) {
      const char = inputString[idx];
      
      if (char === separator || char === '-' || char === ' ') {
        continue;
      } else {
        formattedString += char;
      }
      
      if (i < displayFormat.length && formattedString.length >= separatorLocation) {
        formattedString += separator;
        separatorLocation += displayFormat.toString().charCodeAt(i++) + 1;
      }
    }
    
    return formattedString;
}
  

formatting(filepath);

module.exports = formatter;
