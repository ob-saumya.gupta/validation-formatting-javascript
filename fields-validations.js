"use strict;";
var fs = require('fs');
var readline = require('readline');
var separatorRegex = /[-*\s]/g;
var filepath = "pan-test.csv"; // Replace with the actual file path

function readAndValidateCSVFile(filepath) {
  var rl = readline.createInterface({
    input: fs.createReadStream(filepath),
    crlfDelay: Infinity,
  });

  rl.on('line', (line) => {
    var columns = line.split(',');
    if (columns.length >= 2) {
    //   var key = columns[0].trim();
      var value = columns[0].trim();
      // var minAge = columns[3].trim();
      var expected_outcome = columns[1].trim();
      // var maxAge = columns[4];
      var final = true;

    // We are matching the given string to find out the separator that the user has used,
    // if we find no separator..we ourselves are giving "/" as default separator
    // var match = value.match(/[\/\-. ,]/);
    // if (match){
    //     separator = match[0];
    // }else{
    //     separator = "/";
    // }
    // var status;
    


      // var isValid = IsDateValid(value, separator, minAge, maxAge, final);
    var isValid = isPAN(value, true, 32);
      if(isValid.toString() == expected_outcome.toLowerCase()){
        status = "pass";
      }else{
        status = "fail";
      }
    //   console.log(`name: ${key}, input: ${value}, expected_output: ${expected_outcome}, resultant_output: ${isValid}, status: ${status}`);
console.log(`value: ${value}, expected_output: ${expected_outcome}, resultant_output: ${isValid}, status: ${status}`);    
} else {
      console.log(`Line: ${line} => Invalid: Insufficient columns`);
    }
  });
}

// This function is designed to validate a given date string (inputDateString) based on various criteria.
// inputDateString (string): The input date string that needs to be validated. It represents a date in various formats, such as "dd", "ddMM", or "ddMMyyyy".
// separator (string): The separator character used to format the date in the input date string.
// minAge (number): The minimum age allowed for the date to be considered valid.
// maxAge (number): The maximum age allowed for the date to be considered valid.
// isFinal (boolean): A flag that indicates whether the date validation is considered final.
// Return Value: The function returns a boolean value (true or false) indicating whether the input date string is valid according to the specified criteria.
function IsDateValid(inputDateString, separator, minAge, maxAge, isFinal){
    var result = true;

    var daysInMonths = {
        1: 31, // January
        2: 29, // February 
        3: 31, // March
        4: 30, // April
        5: 31, // May
        6: 30, // June
        7: 31, // July
        8: 31, // August
        9: 30, // September
        10: 31, // October
        11: 30, // November
        12: 31, // December
    };

    //Regex dateRegex = new Regex(@"^[0-9-.\/,_+\s]+$"); // Regex to test for any invalid character
    var dateSeparatorRegex = new RegExp("[-.\\\\/\\s]");
    //if (dateRegex.IsMatch(inputDateString) || (isFinal && inputDateString.Length == 10))

    // cases accounted dd or ddMM or ddMMyyyy
    if (!dateSeparatorRegex.test(inputDateString) && inputDateString.length > 2 && inputDateString.length < 9)
    {
        if (inputDateString.length > 2)
            inputDateString = inputDateString.substring(0, 2) + separator + inputDateString.substring(2);
        if (inputDateString.length > 5)
            inputDateString = inputDateString.substring(0, 5) + separator + inputDateString.substring(5);
    }

    var dateParts = inputDateString.split(dateSeparatorRegex);
    var currentYear = new Date().getFullYear();
    var yearForMinAge = currentYear - minAge; // Replace minAge with the desired minimum age
    var yearForMaxAge = currentYear - maxAge; // Replace maxAge with the desired maximum age

    var month = -1;
    var year = -1;
    result = true;

    if (dateParts.length > 3 || dateParts.length === 0) {
    result = false;
    } 

    if (result) {
        var day;
        day = Number.parseInt(dateParts[0], 10);
        if (result && dateParts.length > 1)
          month = Number.parseInt(dateParts[1], 10);
        if (result && dateParts.length > 2){
          year = Number.parseInt(dateParts[2], 10);
          if(dateParts[2].length != year.toString().length){
            result = false;
          }
        }
      
        if (result && month >= 0) {
          result = daysInMonths.hasOwnProperty(month) && daysInMonths[month] >= day;
        } else{
            result = false;
        }

        if (result && year >= 0) {
          result = checkDate(day, month, year, minAge, maxAge, yearForMinAge, yearForMaxAge);
        }
      }
      return result;
}

// This function is designed to check if a given date (specified by day, month, and year) falls within a specified age range (minAge to maxAge). 
// day (number): The day value of the date.
// month (number): The month value of the date.
// year (number): The year value of the date.
// minAge (number): The minimum age allowed for the date to be considered valid.
// maxAge (number): The maximum age allowed for the date to be considered valid.
// yearForMinAge (number): The calculated year corresponding to the minimum age.
// yearForMaxAge (number): The calculated year corresponding to the maximum age.
// Return Value: The function returns a boolean value (true or false) indicating whether the input date falls within the specified age range.
function checkDate(day, month, year, minAge, maxAge,  yearForMinAge,  yearForMaxAge){
    let result = false;
    var yearAsString = year.toString();
    var length = yearAsString.length;
    year *= 10 ** (4 - length); // Adding zeros to year to make it 4 digits

    if (year <= yearForMinAge && year >= yearForMaxAge) {
    result = true;
    }else if (year < yearForMaxAge) {
        let index = 0;
        let yearTraverse = yearForMaxAge;
        
        while (index < 4 - length) {
          let digit = yearTraverse % 10;
          yearTraverse = Math.floor(yearTraverse / 10);
          year += digit * 10 ** index;
          index += 1;
        }
      
        if (year >= yearForMaxAge) {
          result = true;
        }
      }

      if (result && day === 29 && month === 2) {
        result = false;
        var nextDecade = Math.ceil(year / 10) * 10;
        var iterateTillThisYear = Math.min(nextDecade - year, yearForMinAge - year);
      
        for (let y = year; y <= year + iterateTillThisYear; y++) {
          if ((y % 100 === 0 ? y % 400 === 0 : y % 4 === 0) && y < yearForMinAge) {
            result = true;
            break;
          }
        }
      }

    var currentYear = new Date().getFullYear();
    var currentMonth = new Date().getMonth() + 1; // JavaScript's getMonth() returns 0 for January, so we add 1.
    var currentDay = new Date().getDate();

    let age = currentYear - year;
    if (result && (currentMonth < month || (currentMonth === month && currentDay < day))) {
    age--;
    }

    if (age < minAge || age > maxAge) {
    result = false;
    }
    return result;
}

// This function is designed to perform the Luhn algorithm check on a given input string representing a card number. 
// The Luhn algorithm is used to validate various identification numbers, including credit card numbers.
// inputString (string): The input string representing the card number. It may contain spaces, which will be removed during the validation process.
// Return Value: The function returns a boolean value (true or false) indicating whether the input card number passes the Luhn algorithm check.
function isLuhn(inputString) {
    var cardNumberWithoutSeparator = inputString.replace(/ /g, ''); // Replace all spaces with empty string
    if (cardNumberWithoutSeparator.length === 0) {
      return false;
    }
    var cardNumberLength = cardNumberWithoutSeparator.length;
    let sum = 0;
    let isSecond = false;
  
    for (let i = cardNumberLength - 1; i >= 0; i--) {
      let d = parseInt(cardNumberWithoutSeparator[i], 10);
      if (isSecond) {
        d *= 2;
      }
      sum += Math.floor(d / 10);
      sum += d % 10;
      isSecond = !isSecond;
    }
  
    return sum % 10 === 0;
}

// This function is designed to check whether a given input string (inputString) contains only alphanumeric characters (letters and digits). 
// It also allows specifying a minimum and maximum length for the input string.
// inputString (string): The input string that needs to be checked for alphanumeric characters.
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string. 
// minLength (number, optional): The minimum length allowed for the input string. 
// maxLength (number, optional): The maximum length allowed for the input string.
// Return Value: The function returns a boolean value (true or false) indicating whether the input string contains only alphanumeric characters and satisfies the length constraints.
function isAlphaNumeric(inputString, separatorAscii = 32, minLength = 1, maxLength = 0) {
var alphaNumericRegex = /^[a-zA-Z0-9]+$/; // Regular expression to check for alphanumeric characters
let result = alphaNumericRegex.test(inputString);
var length = inputString.length;

if (result && length < minLength) {
    result = false;
} else if (result && (maxLength > 0 && length > maxLength)) {
    result = false;
}

return result;
}

// This function is designed to check whether a given input string (inputString) contains only numeric characters (digits). 
// It also allows specifying a minimum and maximum length for the input string.
// inputString (string): The input string that needs to be checked for numeric characters.
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string. 
// maxLength (number, optional): The maximum length allowed for the input string. 
// minLength (number, optional): The minimum length allowed for the input string. 
// Return Value: The function returns a boolean value (true or false) indicating whether the input string contains only numeric characters and satisfies the length constraints.
function isNumeric(inputString, separatorAscii = 32, maxLength = 0, minLength = 1) {
    var numericRegex = /^[0-9]+$/; // Regular expression to check for numeric characters
    let result = numericRegex.test(inputString);
    var inputStringLength = inputString.length;
  
    if (result && inputStringLength < minLength) {
      result = false;
    } else if (result && (maxLength > 0 && inputStringLength > maxLength)) {
      result = false;
    }
  
    return result;
}
  
// This function implements the Verhoeff algorithm to check the validity of an Aadhaar number.
// aadhaarNumber (string): The Aadhaar number that needs to be validated.
// Return Value: The function returns a boolean value (true or false) 
// indicating whether the provided Aadhaar number is valid according to the Verhoeff algorithm.
function isVerhoeff(aadhaarNumber) {
    aadhaarNumber = aadhaarNumber.replace(separatorRegex, '');  

    // Dihedral table for digits
    const dTable = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 0, 6, 7, 8, 9, 5],
        [2, 3, 4, 0, 1, 7, 8, 9, 5, 6],
        [3, 4, 0, 1, 2, 8, 9, 5, 6, 7],
        [4, 0, 1, 2, 3, 9, 5, 6, 7, 8],
        [5, 9, 8, 7, 6, 0, 4, 3, 2, 1],
        [6, 5, 9, 8, 7, 1, 0, 4, 3, 2],
        [7, 6, 5, 9, 8, 2, 1, 0, 4, 3],
        [8, 7, 6, 5, 9, 3, 2, 1, 0, 4],
        [9, 8, 7, 6, 5, 4, 3, 2, 1, 0],
    ];

    // Permutation table for digits
    const pTable = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 5, 7, 6, 2, 8, 3, 0, 9, 4],
        [5, 8, 0, 3, 7, 9, 6, 1, 4, 2],
        [8, 9, 1, 6, 0, 4, 3, 5, 2, 7],
        [9, 4, 5, 3, 1, 2, 6, 8, 7, 0],
        [4, 2, 8, 6, 5, 7, 3, 9, 0, 1],
        [2, 7, 9, 3, 8, 0, 6, 4, 1, 5],
        [7, 0, 4, 6, 9, 1, 3, 2, 5, 8],
    ];

    // Reverse the Aadhaar number for the calculation
    const reversedAadhaar = aadhaarNumber.split('').reverse().join('');

    // Initialize checksum
    let checksum = 0;

    // Loop through each digit in the reversed Aadhaar number and calculate the checksum
    for (let i = 0; i < reversedAadhaar.length; i++) {
        const digit = parseInt(reversedAadhaar.charAt(i));
        checksum = dTable[checksum][pTable[i%8][digit]];
    }

    // If the checksum is 0, the Aadhaar number is valid; otherwise, it's not valid
    return checksum === 0;
}

// This function is designed to validate an input string (inputString) 
// representing an amount with up to two decimal places. 
// inputString (string): The input string that needs to be validated as an amount.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state. 
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string. 
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid amount according to the specified conditions.
function isAmount(inputString, isFinal, separatorAscii) {
    var amountTypingRegex = /^[0-9]{0,10}[.]?[0-9]{0,4}?$/; // Regular expression to check for amount format with up to 2 decimal places
    let result = inputString.length > 0 && amountTypingRegex.test(inputString);
  
    if (result && isFinal) {
      var amountFinalRegex = /^[0-9]{1,10}([.][0-9]{1,4})?$/; // Regular expression to check for amount format with up to 2 decimal places and optional thousands separators
      result = amountFinalRegex.test(inputString);
    }
  
    return result;
}

// This function is designed to validate an input string (inputString) as a name. 
// It allows only alphabets (both uppercase and lowercase), spaces, apostrophes, hyphens, and periods. 
// inputString (string): The input string that needs to be validated as a name.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state.
// separatorAscii: This parameter is not used in the function.
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid name according to the specified conditions.
function isName(inputString, isFinal, separatorAscii) {

    var nameTypingRegex = /^[a-z\'\s\.\-]{1,50}$/; // Regular expression to check for name format with only alphabets and spaces
    let result = nameTypingRegex.test(inputString.toLowerCase());
  
    if (result && isFinal) {
      var nameFinalRegex = /^(\s?[a-z]{1,6}(['-]?[a-z]){0,24}[.]?\s?){1,4}$/; // Regular expression to check for name format with only alphabets, spaces, and the first character as uppercase
      result = nameFinalRegex.test(inputString.toLowerCase());
    }
  
    return result;
}

// This function is designed to validate an input string (inputString) as an email address.
// inputString (string): The input string that needs to be validated as an email address.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state. 
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string. 
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid email address according to the specified conditions.
function isEmail(inputString, isFinal, separatorAscii) {
    var emailTypingRegex = /^[a-zA-Z0-9@\-\_\.]{1,64}$/; // Regular expression to check for a valid email format
    let result = emailTypingRegex.test(inputString);
  
    if (result && isFinal) {
      var emailFinalRegexBeforeAt = /^[a-z0-9][a-z0-9\_\-\.]{1,50}$/; // Regular expression to check the part before the "@" symbol in the final email format
      var emailFinalRegexAfterAt = /^([a-z0-9\-]+)(\.[a-z0-9\-]+){1,3}$/; // Regular expression to check the part after the "@" symbol in the final email format
      var emailArray = inputString.split('@');
      result = emailFinalRegexBeforeAt.test(emailArray[0]) && emailFinalRegexAfterAt.test(emailArray[1]);
    }
  
    return result;
}
  
// This function is designed to validate an input string (inputString) as a PAN (Permanent Account Number) card number. 
// It uses another function isAlphaNumeric for basic alphanumeric validation and further performs a 
// specific format check for the PAN card number if the input is in its final state (isFinal).
// inputString (string): The input string that needs to be validated as a PAN card number.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state. 
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string. 
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid PAN card number according to the specified conditions.
function isPAN(inputString, isFinal, separatorAscii) {  
    let result = isAlphaNumeric(inputString, 32, 0, 10);
  
    if (result && isFinal) {
      var panRegex = /^[a-z]{3}[p][a-z]{1}[0-9]{4}[a-z]{1}$/; // Regular expression to check for PAN card format
      result = panRegex.test(inputString.toLowerCase());
    }
  
    return result;
}

// This function is designed to validate an input string (inputString) as an Aadhaar number.
// inputString (string): The input string that needs to be validated as an Aadhaar number.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state. 
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string. 
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid Aadhaar number according to the specified conditions.
function isAadhaar(inputString, isFinal, separatorAscii) {
    inputString = inputString.replace(separatorRegex, '');  
  
    let result = isNumeric(inputString, 16);

  
    if (result && isFinal) {
      result = isVerhoeff(inputString) && (inputString.length === 12 || inputString.length === 16);
    }
  
    return result;
}

// This function is designed to validate an input string (inputString) as a card number. 
// and then performs numeric validation using the isNumeric function.
// inputString (string): The input string that needs to be validated as a card number.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state. 
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string.
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid card number according to the specified conditions.
function isCard(inputString, isFinal, separatorAscii) {
    inputString = inputString.replace(separatorRegex, '');  
  
    let result = isNumeric(inputString, 16, 16, 0);
  
    if (result && isFinal) {
      result = isLuhn(inputString) && inputString.length === 16;
    }
  
    return result;
}

// This function is designed to validate an input string (inputString) as an Indian mobile number. It removes any separators from the input string 
// and then performs numeric validation using the isNumeric function.
// inputString (string): The input string that needs to be validated as an Indian mobile number.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state. 
// separatorAscii (number, optional): The ASCII code of the separator character used in the input string.
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid Indian mobile number according to the specified conditions.
function isIndianMobile(inputString, isFinal, separatorAscii) {
    inputString = inputString.replace(separatorRegex, '');
    
    let result = isNumeric(inputString, 10);
  
    if (result && isFinal) {
      var indianMobileRegex = /^[6-9]\d{9}$/; // Regular expression to check for Indian mobile numbers
      result = indianMobileRegex.test(inputString);
    }
  
    return result;
}
  
// This JavaScript function is designed to validate an input string (inputString) as a One-Time Password (OTP). 
// inputString (string): The input string that needs to be validated as an OTP.
// isFinal (boolean): A boolean flag indicating whether the input string is in its final state. 
// Return Value: The function returns a boolean value (true or false) indicating whether the input string represents a valid OTP according to the specified conditions.
function isValidOtp(inputString, isFinal) {
    let result = isNumeric(inputString, 6);
  
    if (result && isFinal) {
      var otpFinalRegex = /^[0-9]{6}$/; // Regular expression to check for a 6-digit OTP
      result = otpFinalRegex.test(inputString);
    }
  
    return result;
}

  
readAndValidateCSVFile(filepath);